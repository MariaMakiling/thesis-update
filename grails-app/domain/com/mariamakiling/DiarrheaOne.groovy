package com.mariamakiling

class DiarrheaOne {

	boolean diarrhea
	int duration
	boolean bloodStool
	boolean sleepy
	boolean restlessIrritable
	boolean sunkenEyes
	double skin
	String assessment

	static belongsTo = [imciOne: ImciOne]

    static constraints = {
    }
}
