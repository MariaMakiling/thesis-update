package com.mariamakiling

class ImciOne {

	static hasOne = [diarrheaOne: DiarrheaOne, bacterialInfection: BacterialInfection, weightProblem: WeightProblem, 
                        foodIntake: FoodIntake, attachment: Attachment]
	static hasMany = [immunizationOne: ImmunizationOne]

    static constraints = {
    	diarrheaOne nullable: true
    	bacterialInfection nullable: true
    	weightProblem nullable: true
    	foodIntake unique: true
    	attachment unique: true
    }
}
