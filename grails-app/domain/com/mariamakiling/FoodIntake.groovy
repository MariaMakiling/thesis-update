package com.mariamakiling

class FoodIntake {

	boolean breastfed
	String assessment

	static belongsTo = [imciOne: ImciOne]
	static hasOne = [breastfed: Breastfed, notBreastfed: NotBreastfed]
	
    static constraints = {
    	breastfed nullable: true
    	notBreastfed nullable: true
    }
}
