package com.mariamakiling

class DiarrheaTwo {

	boolean diarrhea
	int duration
	boolean bloodStool
	boolean sleepy
	boolean restlessIrritable
	boolean unableDrink
	boolean drinkingEagerly
	boolean sunkenEyes
	int skin
	String assessment

	static belongsTo = [imcitwo: ImciTwo]

    static constraints = {
    }
}
