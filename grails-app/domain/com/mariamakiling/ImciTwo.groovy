package com.mariamakiling

class ImciTwo {

    static hasOne = [cough: Cough, diarrheaTwo: DiarrheaTwo, fever: Fever, measles:
    Measles, dengue: Dengue, vitaminA: VitaminA, malnutritionAnemia:
    MalnutritionAnemia, earProblem: EarProblem, feedingProblem:
    FeedingProblem]

	static hasMany = [generalDangerSigns: GeneralDangerSigns, immunizationTwo: ImmunizationTwo]

    static constraints = {
    	cough nullable: true
    	diarrheaTwo nullable: true
    	fever nullable: true
    	measles nullable: true
    	dengue nullable: true
    	vitaminA nullable: true
    	malnutritionAnemia nullable: true
    	earProblem nullable: true
    	feedingProblem unique: true
    }
}
