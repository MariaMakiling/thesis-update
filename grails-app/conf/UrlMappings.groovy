class UrlMappings {

	static mappings = {

        "/"(view:"/index")
        "500"(view:'/error')

    	//users
        "/api/users"(resources: 'user')
        "/api/patients"(resources: 'patient')
        "/api/healthworkers"(resources: 'healthWorker')
        "/api/admins"(resources: 'admin')

        //imci1
        "/api/imcione" (resources: 'imciOne')
        "/api/weightproblem" (resources: 'weightProblem')
        "/api/immunizationone" (resources: 'immunizationOne')
        "/api/attachment" (resources: 'attachment')
        "/api/diarrheaone" (resources: 'diarrheaOne')
        "/api/bacterialinfection" (resources: 'bacterialInfection')
        "/api/foodintake" (resources: 'foodIntake')
        "/api/breastfed" (resources: 'breastfed')
        "/api/notbreastfed" (resources: 'notBreastfed')

        //imci2
        "/api/imcitwo" (resources: 'imciTwo')
        "/api/generaldangersigns" (resources: 'generalDangerSigns')
        "/api/cough" (resources: 'cough')
        "/api/diarrheatwo" (resources: 'diarrheaTwo')
        "/api/measles" (resources: 'measles')
        "/api/dengue" (resources: 'dengue')
        "/api/fever" (resources: 'fever')

        "/api/earproblem" (resources: 'earProblem')
        "/api/malnutritionanemia" (resources: 'malnutritionAnemia')
        "/api/feedingproblem" (resources: 'feedingProblem')
        "/api/immunizationtwo" (resources: 'immunizationTwo')
        "/api/vitamina" (resources: 'vitaminA')
	}
}
