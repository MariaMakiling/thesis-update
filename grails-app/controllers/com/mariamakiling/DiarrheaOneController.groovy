package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class DiarrheaOneController {

    def index() {
    	render DiarrheaOne.list() as JSON
    }

    def save() {
    	def newDiarrheaOne = new DiarrheaOne(request.JSON)
    	newDiarrheaOne.save(failOnError: true)
        println request.JSON
    	render(['success': true] as JSON)
    }

    def show() {
    	def diarrheaOne = DiarrheaOne.get(params.id)

    	render diarrheaOne as JSON
    }
}
