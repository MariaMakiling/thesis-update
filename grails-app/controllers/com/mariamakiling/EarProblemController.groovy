package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class EarProblemController {

    def index() { 
    	render EarProblem.list() as JSON
    }

    def save() {
    	def newEarProblem= new EarProblem(request.JSON)
    	newEarProblem.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def earProblem = EarProblem.get(params.id)

    	render earProblem as JSON
    }
}
