package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class WeightProblemController {

	static allowedMethods = [save: "POST"]

    def index() { 
    	render WeightProblem.list as JSON
    }

    def save() {
    	def newWeightProblem = new WeightProblem(request.JSON)
    	newWeightProblem.save()
        println request.JSON
    	render(['success': true] as JSON)
    }

    def show() {
    	def weightProblem = WeightProblem.get(params.id)

    	render weightProblem as JSON
    }
}
