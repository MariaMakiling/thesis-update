package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class MeaslesController {

    def index() { 
    	render Measles.list() as JSON
    }

    def save() {
    	def newMeasles = new Measles(request.JSON)
    	newMeasles.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def measles = Measles.get(params.id)

    	render measles as JSON
    }
}
