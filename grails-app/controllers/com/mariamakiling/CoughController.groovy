package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class CoughController {

    def index() { 
    	render Cough.list() as JSON
    }

    def save() {
    	def newCough = new Cough(request.JSON)
    	newCough.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def cough = Cough.get(params.id)

    	render cough as JSON
    }
}
