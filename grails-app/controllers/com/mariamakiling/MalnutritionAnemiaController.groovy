package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class MalnutritionAnemiaController {

    def index() { 
    	render MalnutritionAnemia.list() as JSON
    }

    def save() {
    	def newMalnutritionAnemia= new MalnutritionAnemia(request.JSON)
    	newMalnutritionAnemia.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def malnutritionAnemia = MalnutritionAnemia.get(params.id)

    	render malnutritionAnemia as JSON
    }
}
