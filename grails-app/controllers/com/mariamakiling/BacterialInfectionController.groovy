package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class BacterialInfectionController {

    def index() { 
    	render BacterialInfection.list() as JSON
    }

    def save() {
    	def newBacterialInfection = new BacterialInfection(request.JSON)
    	newBacterialInfection.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def bacterialInfection = BacterialInfection.get(params.id)

    	render bacterialInfection as JSON
    }
}
