package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class ImciTwoController {

    def index() {
    	render ImciTwo.list() as JSON
    }

    def save() {
        def newImciTwo = new Patient(request.JSON)
        if(newImciTwo.validate()){
            newImciTwo.save()
            println request.JSON
            render(["success":true] as JSON)
        } else {
            response.status = 500
            render(newImciTwo.errors as JSON)
        }

    }

    def show() {
    	def imciTwo = ImciTwo.get(params.id)

    	render imciTwo as JSON
    }
}
