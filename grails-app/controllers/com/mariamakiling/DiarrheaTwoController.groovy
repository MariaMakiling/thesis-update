package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class DiarrheaTwoController {

    def index() {
    	render DiarrheaTwo.list() as JSON
    }

    def save() {
    	def newDiarrheaTwo = new DiarrheaTwo(request.JSON)
    	newDiarrheaTwo.save()
        println request.JSON
    	render(['success': true] as JSON)
    }

    def show() {
    	def diarrheaTwo = DiarrheaTwo.get(params.id)

    	render diarrheaTwo as JSON
    }
}
