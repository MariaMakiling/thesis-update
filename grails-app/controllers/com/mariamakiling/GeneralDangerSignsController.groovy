package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class GeneralDangerSignsController {

    def index() { 
    	render GeneralDangerSigns.list() as JSON
    }

    def save() {
    	def newGeneralDangerSigns = new GeneralDangerSigns(request.JSON)
    	newGeneralDangerSigns.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def generalDangerSigns = GeneralDangerSigns.get(params.id)

    	render generalDangerSigns as JSON
    }
}
