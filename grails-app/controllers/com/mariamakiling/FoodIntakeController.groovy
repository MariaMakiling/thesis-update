package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class FoodIntakeController {

    def index() { 
    	render FoodIntake.list() as JSON
    }

    def save() {
    	def newFoodIntake = new FoodIntake(request.JSON)
    	newFoodIntake.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def foodIntake = FoodIntake.get(params.id)

    	render foodIntake as JSON
    }
}
