package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')

class ImciOneController {

    def index() {
    	render ImciOne.list() as JSON
    }

    def save() {
        def newImciOne = new ImciOne(request.JSON)
        newImciOne.save()
        println request.JSON
        render(['success': true] as JSON)
        //if(newImciOne.validate()){
        //    newImciOne.save()
        //    println request.JSON
        //    render(["success":true] as JSON)
        //} else {
        //    response.status = 500
        //    render(newImciOne.errors as JSON)
        //}

    }

    def show() {
    	def imciOne = ImciOne.get(params.id)

    	render imciOne as JSON
    }
}
