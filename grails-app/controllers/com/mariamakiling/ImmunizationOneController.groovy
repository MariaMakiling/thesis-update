package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class ImmunizationOneController {

    def index() { 
    	render ImmunizationOne.list() as JSON
    }

    def save() {
    	def newImmunizationOne = new ImmunizationOne(request.JSON)
    	newImmunizationOne.save()
        println request.JSON
    	render(['success': true] as JSON)
    }

    def show() {
    	def immunizationOne = ImmunizationOne.get(params.id)

    	render immunizationOne as JSON
    }
}
