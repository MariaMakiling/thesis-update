package com.mariamakiling

import grails.converters.JSON
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class AttachmentController {

    def index() { 
    	render Attachment.list() as JSON
    }

    def save() {
    	def newAttachment = new Attachment(request.JSON)
    	newAttachment.save()

    	render(['success': true] as JSON)
    }

    def show() {
    	def attachment = Attachment.get(params.id)

    	render attachment as JSON
    }
}
