app.controller('Login', function($scope, store, $state, loginFactory){
	$scope.user = {}
		$scope.login = function(){
			loginFactory.save($scope.user,function(success){
				store.set('jwt', success.access_token)
				store.set('username', success.username)
				store.set('roles', success.roles)
				$state.go('home')
				console.log("login");
			}, function (error) {
				$scope.user = {}
			})
		}
})