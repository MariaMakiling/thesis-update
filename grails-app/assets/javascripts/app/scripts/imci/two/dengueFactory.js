app.factory('dengueFactory', ['$resource', function ($resource) {
    return $resource('api/dengue',
        {
            'update': {method: 'PUT'}
        })
}])