app.factory('generalDangerSignsFactory', ['$resource', function ($resource) {
    return $resource('api/generaldangersign',
        {
            'update': {method: 'PUT'}
        })
}])