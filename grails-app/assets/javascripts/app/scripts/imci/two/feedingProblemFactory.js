app.factory('feedingProblemFactory', ['$resource', function ($resource) {
    return $resource('api/feedingProblem',
        {
            'update': {method: 'PUT'}
        })
}])