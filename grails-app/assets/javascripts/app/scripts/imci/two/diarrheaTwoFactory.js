app.factory('diarrheaTwoFactory', ['$resource', function ($resource) {
    return $resource('api/diarrhea',
        {
            'update': {method: 'PUT'}
        })
}])