app.controller('Dengue', function($scope, store, $state, dengueFactory){

	$scope.dengue = {}
		$scope.saveDengue = function() {
			$scope.dengue.assessment = $scope.getDengue();
			console.log($scope.dengue);
			dengueFactory.save($scope.dengue,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getDengue = function() {
		var result = "";
		if ($scope.dengue.coldExtremeties || $scope.dengue.black 
			|| $scope.dengue.abdominalPain || $scope.dengue.skinPetechiae || $scope.dengue.bleeding || $scope.dengue.vomiting){
			result = "dengue hemorrahagic fever";
		}else if ($scope.dengue.abdominalPain || $scope.dengue.skinPetechiae){
			result = "possible dengue hemorrahagic fever";
		} 
		return result;
	};	
})