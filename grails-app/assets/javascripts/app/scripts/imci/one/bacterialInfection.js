app.controller('BacterialInfection', function($scope, store, $state, bacterialInfectionFactory){

	$scope.bacterialInfection = {}

		$scope.saveBacterialInfection = function() {
			$scope.bacterialInfection.assessment = $scope.getBacterialInfection();
			console.log($scope.bacterialInfection);
			bacterialInfectionFactory.save($scope.bacterialInfection,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

	$scope.getBacterialInfection = function() {
		var result = "";
		if ($scope.bacterialInfection.feeding || $scope.bacterialInfection.convulsion || $scope.bacterialInfection.chestIndrawing 
			|| $scope.bacterialInfection.temperature || $scope.bacterialInfection.movement){
			result = "very severe disease";
		}else if ($scope.bacterialInfection.umnbilicus && $scope.bacterialInfection.skinPustules){
			result = "local bacterial infection";
		}else {
			result = "severe disease or local infection unlikely";
		}
		return result;
	};	
})