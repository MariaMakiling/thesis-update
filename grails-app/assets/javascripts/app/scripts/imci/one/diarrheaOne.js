app.controller('DiarrheaOne', function($scope, store, $state, diarrheaOneFactory){
	
	$scope.diarrheaOne = {}
		$scope.saveDiarrheaOne = function() {
			$scope.diarrheaOne.assessment = $scope.getDiarrheaOne();
			console.log($scope.diarrheaOne);
			diarrheaOneFactory.save($scope.diarrheaOne,function (result){
				console.log(result)
			}, function (error) {
				console.log(error)
			})
		}

		$scope.getDiarrheaOne = function() {
		var result = "";
		if ($scope.diarrheaOne.sunkenEyes && $scope.diarrheaOne.skin > 2){
			result = "severe dehydration";
		}else if ($scope.diarrheaOne.restlessIrritable || $scope.diarrheaOne.sunkenEyes && $scope.diarrheaOne.skin > 2){
			result = "some dehydration";
		}else {
			result = "no dehydration";
		}
		return result;
	};	
})