app.factory('imciOneFactory', ['$resource', function ($resource) {
    return $resource('api/imcione',
        {
            'create': {method: 'POST'}
        })
}])