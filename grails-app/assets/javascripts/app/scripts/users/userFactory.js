app.factory('userFactory', ['$resource', function ($resource) {
    return $resource('api/patient',
        {
            'update': {method: 'PUT'}
        })
}])