package com.mariamakiling

class mainClass {
    static void main(String... args){
        def file = this.getClass().getResource('/test.csv').getFile()
        def db = TransactionDBLoader.loadFromCsv(file, /,/)

        def frequent = Apriori.mineFrequentPattern(db,0.20)
        def association = AssociationRules.mineAssociationRules(frequent,0.60)
        //def getF = Apriori.get1Itemset(frequent).toString()

        println("Frequent Conditions on IMCI and Associated Key Items Found")

        println("Number of Frequent Items Sets: " + frequent.size())
        for (String item : frequent) {
            println(item)
        }
        println("Number of Association Rules: " + association.size())
        for (String item : association) {
            println(item)
        }
    }
}
